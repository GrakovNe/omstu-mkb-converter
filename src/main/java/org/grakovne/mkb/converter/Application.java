package org.grakovne.mkb.converter;

import org.grakovne.mkb.converter.common.constants.StringConstants;
import org.grakovne.mkb.converter.dto.export.MkbContent;
import org.grakovne.mkb.converter.parser.XlsxParser;
import org.grakovne.mkb.converter.serializer.MkbSerializer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {

        String dbName = askUserDbName();
        String authorName = askUserAuthorName();
        String excelFileName = askUserFilePath();

        XlsxParser parser = new XlsxParser(getFile(excelFileName));

        MkbContent content = parser.parseFile(dbName, authorName);
        MkbSerializer serializer = new MkbSerializer(content);

        writeContent(serializer.serialize());
    }

    private static String askUserDbName() {
        Scanner scanner = new Scanner(System.in);
        System.out.print(StringConstants.getLineBreak());
        System.out.print(StringConstants.getDataBaseNameMessage());
        return scanner.next();
    }

    private static String askUserAuthorName() {
        Scanner scanner = new Scanner(System.in);
        System.out.print(StringConstants.getLineBreak());
        System.out.print(StringConstants.getAuthor());
        return scanner.next();
    }

    private static String askUserFilePath() {
        Scanner scanner = new Scanner(System.in);
        System.out.print(StringConstants.getLineBreak());
        System.out.print(StringConstants.getExcelFileName());
        return scanner.next();
    }

    private static File getFile(String pathToFile) {
        File file = new File(pathToFile);

        if (!file.exists()) {
            System.exit(-1);
        }

        return file;
    }

    private static void writeContent(String content) {
        try {
            FileWriter writer = new FileWriter("out.mkb");
            writer.write(content);
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException("can't create out file");
        }
    }
}
