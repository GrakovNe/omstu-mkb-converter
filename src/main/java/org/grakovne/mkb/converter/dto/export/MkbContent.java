package org.grakovne.mkb.converter.dto.export;

import org.grakovne.mkb.converter.entity.Opinion;
import org.grakovne.mkb.converter.entity.Question;

import java.util.List;

public class MkbContent {

    private String name;
    private String author;

    private List<Question> questions;
    private List<Opinion> opinions;
    private Double posterioriProbability;

    public MkbContent(String name, String author, List<Question> questions, List<Opinion> opinions, Double posterioriProbability) {
        this.name = name;
        this.author = author;
        this.questions = questions;
        this.opinions = opinions;
        this.posterioriProbability = posterioriProbability;
    }

    public MkbContent(String name, String author) {
        this.name = name;
        this.author = author;
    }

    public MkbContent() {
    }

    public Double getPosterioriProbability() {
        return posterioriProbability;
    }

    public void setPosterioriProbability(Double posterioriProbability) {
        this.posterioriProbability = posterioriProbability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public List<Opinion> getOpinions() {
        return opinions;
    }

    public void setOpinions(List<Opinion> opinions) {
        this.opinions = opinions;
    }

    @Override
    public String toString() {
        return "MkbContent{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", questions=" + questions +
                ", opinions=" + opinions +
                ", posterioriProbability=" + posterioriProbability +
                '}';
    }
}
