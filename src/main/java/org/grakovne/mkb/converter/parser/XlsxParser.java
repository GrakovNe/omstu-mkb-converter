package org.grakovne.mkb.converter.parser;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.grakovne.mkb.converter.common.constants.ExcelConstants;
import org.grakovne.mkb.converter.dto.export.MkbContent;
import org.grakovne.mkb.converter.entity.Opinion;
import org.grakovne.mkb.converter.entity.Option;
import org.grakovne.mkb.converter.entity.Probability;
import org.grakovne.mkb.converter.entity.Question;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XlsxParser {

    private XSSFSheet sheet;

    public XlsxParser(File file) {
        try (XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(file))) {
            sheet = myExcelBook.getSheetAt(ExcelConstants.getActiveSheet());
        } catch (IOException e) {
            throw new IllegalArgumentException("can't open file");
        }
    }

    public MkbContent parseFile(String name, String author) {
        MkbContent content = new MkbContent();

        content.setQuestions(parseQuestions());
        content.setPosterioriProbability(countPosterioriProbability());
        content.setOpinions(parseOpinions());
        content.setName(name);
        content.setAuthor(author);

        return content;
    }

    private List<Question> parseQuestions() {
        XSSFRow row = sheet.getRow(ExcelConstants.getQuestionsRowNumber());
        List<Question> result = new ArrayList<>();

        Integer columnCounter = ExcelConstants.getFirstQuestionColumnNumber();

        for (int i = columnCounter; i < row.getLastCellNum(); i++) {
            String questionValue = row.getCell(i).getStringCellValue();
            if (!questionValue.isEmpty()) {
                result.add(new Question(questionValue));
            }
        }

        return result;
    }

    private List<Option> parseOptions() {
        Integer rowCounter = ExcelConstants.getFirstOptionRowNumber();
        List<Option> result = new ArrayList<>();


        String optionValue;
        do {
            XSSFRow row = sheet.getRow(rowCounter);
            optionValue = row.getCell(ExcelConstants.getOptionsColumnNumber()).getStringCellValue();

            if (!optionValue.isEmpty()) {
                result.add(new Option(optionValue));
            }

            rowCounter++;

        } while (!optionValue.isEmpty());

        return result;
    }


    private List<Opinion> parseOpinions() {
        List<Question> questions = parseQuestions();
        List<Option> options = parseOptions();
        List<Opinion> result = new ArrayList<>(options.size() * questions.size());


        for (int i = 0; i < options.size(); i++) {
            XSSFRow row = sheet.getRow(i + ExcelConstants.getFirstOptionRowNumber());
            Option option = options.get(i);

            Integer questionNumber = 0;
            for (int j = 1; j < row.getLastCellNum(); j += 2) {
                Double positiveProbability = row.getCell(j).getNumericCellValue();
                Double negativeProbability = row.getCell(j + 1).getNumericCellValue();
                Probability probability = new Probability(positiveProbability, negativeProbability);

                Opinion opinion = new Opinion();
                opinion.setProbability(probability);
                opinion.setOption(option);

                opinion.setQuestion(questions.get(questionNumber));
                questionNumber++;
                result.add(opinion);
            }
        }

        return result;
    }

    private strictfp Double countPosterioriProbability() {
        return 1.0 / parseOptions().size();
    }
}
