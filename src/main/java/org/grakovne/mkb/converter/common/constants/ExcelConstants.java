package org.grakovne.mkb.converter.common.constants;

public class ExcelConstants {
    private final static Integer ACTIVE_SHEET = 0;
    private final static Integer FIRST_QUESTION_COLUMN_NUMBER = 1;
    private final static Integer QUESTIONS_ROW_NUMBER = 0;

    private final static Integer OPTIONS_COLUMN_NUMBER = 0;
    private final static Integer FIRST_OPTION_ROW_NUMBER = 2;

    public static Integer getActiveSheet() {
        return ACTIVE_SHEET;
    }

    public static Integer getFirstQuestionColumnNumber() {
        return FIRST_QUESTION_COLUMN_NUMBER;
    }

    public static Integer getQuestionsRowNumber() {
        return QUESTIONS_ROW_NUMBER;
    }

    public static Integer getFirstOptionRowNumber() {
        return FIRST_OPTION_ROW_NUMBER;
    }

    public static Integer getOptionsColumnNumber() {
        return OPTIONS_COLUMN_NUMBER;
    }
}
