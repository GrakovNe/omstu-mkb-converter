package org.grakovne.mkb.converter.common.constants;

public class StringConstants {
    private final static String AUTHOR = "Автор: ";
    private final static String QUESTIONS = "Вопросы:";
    private final static String DATA_BASE_NAME_MESSAGE = "Имя Базы знаний: ";
    private final static String EXCEL_FILE_NAME = "Путь к Excel - файлу: ";
    private final static String LINE_BREAK = "\n";
    private final static String DOT_SYMBOL = ".";
    private final static String SPACE_SYMBOL = " ";
    private final static String COMMA_SYMBOL = ",";

    public static String getAuthor() {
        return AUTHOR;
    }

    public static String getLineBreak() {
        return LINE_BREAK;
    }

    public static String getQuestions() {
        return QUESTIONS;
    }

    public static String getDotSymbol() {
        return DOT_SYMBOL;
    }

    public static String getSpaceSymbol() {
        return SPACE_SYMBOL;
    }

    public static String getCommaSymbol() {
        return COMMA_SYMBOL;
    }

    public static String getDataBaseNameMessage(){
        return DATA_BASE_NAME_MESSAGE;
    }

    public static String getExcelFileName(){
        return EXCEL_FILE_NAME;
    }
}
