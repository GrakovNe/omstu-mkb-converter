package org.grakovne.mkb.converter.serializer;

import org.grakovne.mkb.converter.common.constants.StringConstants;
import org.grakovne.mkb.converter.dto.export.MkbContent;
import org.grakovne.mkb.converter.entity.Opinion;
import org.grakovne.mkb.converter.entity.Option;
import org.grakovne.mkb.converter.entity.Question;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MkbSerializer {

    private MkbContent content;

    public MkbSerializer(MkbContent content) {
        this.content = content;
    }

    public String serialize() {
        StringBuilder builder = new StringBuilder();

        builder
                .append(serializeName())
                .append(serializeLineBreak())
                .append(serializeAuthor())
                .append(serializeLineBreak())
                .append(serializeLineBreak())
                .append(serializeQuestionHeader())
                .append(serializeLineBreak())
                .append(serializeLineBreak());

        content.getQuestions().forEach(question -> builder
                .append(serializeQuestion(question))
                .append(serializeLineBreak())
        );

        builder
                .append(serializeOpinions(groupOpinionsByOption()));

        return builder.toString().trim();
    }

    private Map<Option, List<Opinion>> groupOpinionsByOption() {
        return content.getOpinions().stream().collect(Collectors.groupingBy(Opinion::getOption));
    }

    private String serializeOpinions(Map<Option, List<Opinion>> map) {
        StringBuilder builder = new StringBuilder();

        map.forEach((option, opinions) -> {

            builder
                    .append(StringConstants.getLineBreak())
                    .append(option.getValue())
                    .append(StringConstants.getCommaSymbol())
                    .append(content.getPosterioriProbability())
                    .append(StringConstants.getCommaSymbol())
                    .append(StringConstants.getSpaceSymbol());

            for (int i = 0; i < opinions.size(); i++) {
                builder
                        .append(i + 1)
                        .append(StringConstants.getCommaSymbol())
                        .append(opinions.get(i).getProbability().getPositiveProbability())
                        .append(StringConstants.getCommaSymbol())
                        .append(opinions.get(i).getProbability().getNegativeProbability());

                if (i + 1 != opinions.size()) {
                    builder
                            .append(StringConstants.getCommaSymbol())
                            .append(StringConstants.getSpaceSymbol());
                }
            }

        });

        return builder.toString();
    }

    private String serializeQuestionHeader() {
        return StringConstants.getQuestions();
    }

    private String serializeName() {
        return content.getName() + StringConstants.getDotSymbol();
    }

    private String serializeAuthor() {
        return StringConstants.getAuthor() + content.getAuthor() + StringConstants.getDotSymbol();
    }

    private String serializeLineBreak() {
        return StringConstants.getLineBreak();
    }

    private String serializeQuestion(Question question) {
        return question.getValue();
    }
}
