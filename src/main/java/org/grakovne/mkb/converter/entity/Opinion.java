package org.grakovne.mkb.converter.entity;

public class Opinion {
    private Question question;
    private Option option;
    private Probability probability;

    public Opinion(Question question, Option option, Probability probability) {
        this.question = question;
        this.option = option;
        this.probability = probability;
    }

    public Opinion() {
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }

    public Probability getProbability() {
        return probability;
    }

    public void setProbability(Probability probability) {
        this.probability = probability;
    }

    @Override
    public String toString() {
        return "Opinion{" +
                "question=" + question +
                ", option=" + option +
                ", probability=" + probability +
                '}';
    }
}
