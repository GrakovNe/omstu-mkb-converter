package org.grakovne.mkb.converter.entity;

public class Probability {

    private Double positiveProbability;
    private Double negativeProbability;

    public Probability(Double positiveProbability, Double negativeProbability) {
        this.positiveProbability = positiveProbability;
        this.negativeProbability = negativeProbability;
    }

    public Double getPositiveProbability() {
        return positiveProbability;
    }

    public void setPositiveProbability(Double positiveProbability) {
        this.positiveProbability = positiveProbability;
    }

    public Double getNegativeProbability() {
        return negativeProbability;
    }

    public void setNegativeProbability(Double negativeProbability) {
        this.negativeProbability = negativeProbability;
    }

    @Override
    public String toString() {
        return "Probability{" +
                "positiveProbability=" + positiveProbability +
                ", negativeProbability=" + negativeProbability +
                '}';
    }
}
