package org.grakovne.mkb.converter.entity;

import java.util.Objects;

public class Question {
    private String value;

    public Question(String value) {
        this.value = value;
    }

    public Question() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Question{" +
                "value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return Objects.equals(value, question.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(value);
    }
}
