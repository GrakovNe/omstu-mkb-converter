package org.grakovne.mkb.converter.entity;

import java.util.Objects;

public class Option {
    private String value;

    public Option(String value) {
        this.value = value;
    }

    public Option() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Option{" +
                "value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Option option = (Option) o;
        return Objects.equals(value, option.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(value);
    }
}
